import Sofa
from Sofa.constants import *
import numpy as np

def createScene(rootNode):
    plugins = rootNode.addChild('Plugins')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.AnimationLoop')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.Constraint.Lagrangian.Solver')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Iterative')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.Mapping.Linear')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.Mapping.NonLinear')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.Mass')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.ODESolver.Backward')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.SolidMechanics.FEM.Elastic')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.StateContainer')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.Topology.Container.Constant')
    plugins.addObject('RequiredPlugin', name='Sofa.Component.Visual')
    plugins.addObject('RequiredPlugin', name='Sofa.GL.Component.Rendering3D')
    plugins.addObject('RequiredPlugin', name='SofaConstraint')
    plugins.addObject('RequiredPlugin', name='SofaCutting')

    rootNode.addObject('FreeMotionAnimationLoop', parallelCollisionDetectionAndFreeMotion=True)
    rootNode.addObject('LCPConstraintSolver', tolerance=1e-3, maxIt=1000)
    rootNode.addObject('DefaultPipeline', depth=6)
    rootNode.addObject('BruteForceBroadPhase')
    rootNode.addObject('BVHNarrowPhase')
    rootNode.addObject('MinProximityIntersection', alarmDistance='0.2', contactDistance='0.1')
    rootNode.addObject('DefaultContactManager', response='CuttingContactConstraint')
    rootNode.addObject('DefaultVisualManagerLoop')

    liver = rootNode.addChild('Liver')
    liver.addObject('EulerImplicitSolver')
    liver.addObject('CGLinearSolver', iterations=15, threshold=1e-15, tolerance=1e-9)
    liver.addObject('MeshGmshLoader', name='loader', filename='mesh/liver.msh')
    liver.addObject('TetrahedronSetTopologyContainer', src='@loader', name='Topology', tags=' ')
    liver.addObject('TetrahedronSetGeometryAlgorithms')
    liver.addObject('TetrahedronSetTopologyModifier')
    liver.addObject('MechanicalObject', name='DOFs', template='Vec3d', src='@loader')
    liver.addObject('UniformMass', totalMass='1.0')
    liver.addObject('TetrahedronFEMForceField', name='FEM', youngModulus='300', poissonRatio='0.45', method='large')
    liver.addObject('PrecomputedConstraintCorrection')

    liver.addObject('BoxROI', name='AttachmentROI', box='-1 3 -1 1 5 1', drawBoxes='1')
    liver.addObject('FixedConstraint', indices='@AttachmentROI.indices')

    liver.addObject('LineCollisionModel')
    liver.addObject('CuttingProcessManager')


    instrument = rootNode.addChild('Instrument')
    instrument.addObject('EulerImplicitSolver')
    instrument.addObject('CGLinearSolver', iterations=15, threshold=1e-15, tolerance=1e-9)
    mechObj = instrument.addObject('MechanicalObject', name='DOFs', template='Rigid3d')
    instrument.addObject('UncoupledConstraintCorrection')
    instrument.addObject(InstrumentController(name='Instrument', dof=mechObj))

    instrument_collision = instrument.addChild('Collision')
    instrument_collision.addObject('Mesh', name='Mesh', position='0 0 0 0 0 3', edges='0 1')
    instrument_collision.addObject('MechanicalObject', name='DOFs', src='@./Mesh', template='Vec3d')
    instrument_collision.addObject('LineCollisionModel')
    instrument_collision.addObject('RigidMapping')
    instrument_collision.addObject('CuttingProcessManager')

    
class InstrumentController(Sofa.Core.Controller):
    def __init__(self, *args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
        self.dof = kwargs["dof"]

    def move(self, translation):
        pos = self.dof.position[0]
        pos[0] += translation[0]
        pos[1] += translation[1]
        pos[2] += translation[2]
        self.dof.position = pos

    def init(self):
        self.functionDict = { 
	    # Arrows
            Key.leftarrow : lambda:[0.1, 0.0, 0.0],
	    Key.rightarrow : lambda:[-0.1, 0.0, 0.0],
	    Key.uparrow : lambda:[0.0, 0.1, 0.0],
	    Key.downarrow : lambda:[0.0, -0.1, 0.0],
        }

    def onKeypressedEvent(self, event):
        if event['key'] in self.functionDict:
            self.move(self.functionDict[event['key']]())
        else:
            print('The key is not in the list')
    

