#include "CuttingProcessManager.h"

#include <sofa/core/ObjectFactory.h>

namespace sofa::component::topology::utility
{
  int CuttingProcessManagerClass = core::RegisterObject("Cutting process manager").add< CuttingProcessManager >();

  CuttingProcessManager::CuttingProcessManager()
    : m_intersectedEdges( initData(&m_intersectedEdges, "intersectedEdges", "List of edge IDs that were intersected during the cut" ))
  {
  }

  CuttingProcessManager::~CuttingProcessManager()
  {
  }
    
}
