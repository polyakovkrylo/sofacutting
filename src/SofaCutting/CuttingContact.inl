#pragma once

#include "CuttingProcessManager.h"
#include "CuttingContact.h"

#include <algorithm>
#include <iostream>


namespace sofa::component::collision::response::contact
{

template < class TCollisionModel1, class TCollisionModel2  >
CuttingContact<TCollisionModel1,TCollisionModel2>::CuttingContact()
  : CuttingContact(nullptr, nullptr, nullptr)
{
}


template < class TCollisionModel1, class TCollisionModel2 >
CuttingContact<TCollisionModel1,TCollisionModel2 >::CuttingContact(CollisionModel1* model1, CollisionModel2* model2, Intersection* intersectionMethod)
    : model1(model1)
    , model2(model2)
    , parent(nullptr)
{
  
}

template < class TCollisionModel1, class TCollisionModel2 >
CuttingContact<TCollisionModel1,TCollisionModel2 >::~CuttingContact()
{
}

template < class TCollisionModel1, class TCollisionModel2 >
void CuttingContact<TCollisionModel1,TCollisionModel2 >::cleanup()
{
}


template < class TCollisionModel1, class TCollisionModel2 >
void CuttingContact<TCollisionModel1,TCollisionModel2>::setDetectionOutputs(OutputVector* o)
{
  TOutputVector& outputs = *static_cast<TOutputVector*>(o);

  // we only take the latest intersection for each element
  for (auto &d : outputs) {
    auto it = std::find_if(detections.begin(), detections.end(), [&](DetectionOutput &o) {
      return (o.elem.first == d.elem.first) && (o.elem.second == d.elem.second);
    });

    if (it == detections.end()) {
      // If the intersection is unique, add it to the vector
      detections.push_back(d);
    }
    else {
      // Otherwise replace it with the latest one
      *it = d;
    }

  }
}

template < class TCollisionModel1, class TCollisionModel2 >
void CuttingContact<TCollisionModel1,TCollisionModel2 >::createResponse(core::objectmodel::BaseContext* group)
{
  if (parent!=nullptr) {
    parent->removeObject(this);
  }
  
  parent = group;
  if (parent!=nullptr) {
    parent->addObject(this);
  }

  sofa::component::topology::utility::CuttingProcessManager *man1 = nullptr;
  sofa::component::topology::utility::CuttingProcessManager *man2 = nullptr;
  model1->getContext()->get(man1);
  model2->getContext()->get(man2);
  
  if (man1 || man2) {
    msg_error() << "adding to manager";
    if (man1) {
      auto vec = man1->m_intersectedEdges.beginEdit();
      vec->clear();
      for (auto &o : detections) {
	vec->push_back(o.elem.first.getIndex());	
      }
      man1->m_intersectedEdges.endEdit();
    }
    if (man2) {
      auto vec = man2->m_intersectedEdges.beginEdit();
      vec->clear();
      for (auto &o : detections) {
	vec->push_back(o.elem.second.getIndex());	
      }
      man2->m_intersectedEdges.endEdit();
    }
 
  }
}

template < class TCollisionModel1, class TCollisionModel2 >
void CuttingContact<TCollisionModel1,TCollisionModel2>::removeResponse()
{
}

} //namespace sofa::component::collision::response::contact
