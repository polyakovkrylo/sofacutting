#pragma once

#include <sofa/helper/config.h>

#ifdef SOFA_BUILD_SOFACUTTING
#define SOFA_SofaCutting_API SOFA_EXPORT_DYNAMIC_LIBRARY
#else
#define SOFA_SofaCutting_API SOFA_IMPORT_DYNAMIC_LIBRARY
#endif

/**
Description of the plugin
*/
