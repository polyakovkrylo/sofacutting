#pragma once

#include "initSofaCutting.h"

#include <sofa/core/objectmodel/BaseObject.h>

namespace sofa::component::topology::utility
{
  
  class SOFA_SofaCutting_API CuttingProcessManager : public core::objectmodel::BaseObject
  {
  public:
    SOFA_CLASS(CuttingProcessManager, core::objectmodel::BaseObject);

    typedef typename sofa::Index Index;

    // Data < type::vector <Index> > m_intersectedPoints;
    Data < type::vector <Index> > m_intersectedEdges;

  protected:
    CuttingProcessManager();

    ~CuttingProcessManager() override;

  };
    
}
