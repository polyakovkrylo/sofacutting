#include "initSofaCutting.h"

extern "C" {
  void initExternalModule()
  {

  }

  const char* getModuleName()
  {
    return "SofaCutting";
  }

  const char* getModuleVersion()
  {
    return "0.1";
  }

  const char* getModuleLicense()
  {
    return "LGPL";
  }

  const char* getModuleDescription()
  {
    return "A plugin implementing cutting of deformable object";
  }

  const char* getModuleComponentList()
  {
    return "";
  }
}
