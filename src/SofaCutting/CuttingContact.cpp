#include <CuttingContact.inl>

#include <sofa/component/collision/geometry/PointModel.h>
#include <sofa/component/collision/geometry/LineModel.h>

using sofa::core::collision::Contact;
using sofa::defaulttype::Vec3Types;
using sofa::helper::Creator;
using sofa::component::collision::geometry::PointCollisionModel;
using sofa::component::collision::geometry::LineCollisionModel;

namespace sofa::component::collision::response::contact
{
  Creator<Contact::Factory, CuttingContact<PointCollisionModel<Vec3Types>, PointCollisionModel<Vec3Types>> > PointPointCuttingContactClass("CuttingContactConstraint",true);
  Creator<Contact::Factory, CuttingContact<LineCollisionModel<Vec3Types>, PointCollisionModel<Vec3Types>> > LinePointCuttingContactClass("CuttingContactConstraint",true);
  Creator<Contact::Factory, CuttingContact<LineCollisionModel<Vec3Types>, LineCollisionModel<Vec3Types>> > LineLineCuttingContactClass("CuttingContactConstraint",true);

template class SOFA_SofaCutting_API CuttingContact<PointCollisionModel<Vec3Types>, PointCollisionModel<Vec3Types>>;
template class SOFA_SofaCutting_API CuttingContact<LineCollisionModel<Vec3Types>, PointCollisionModel<Vec3Types>>;
template class SOFA_SofaCutting_API CuttingContact<LineCollisionModel<Vec3Types>, LineCollisionModel<Vec3Types>>;

} //namespace sofa::component::collision::response::contact
