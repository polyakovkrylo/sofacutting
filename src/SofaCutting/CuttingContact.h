#pragma once
#include "initSofaCutting.h"

#include <sofa/helper/Factory.h>
#include <sofa/core/collision/Contact.h>
#include <sofa/core/collision/Intersection.h>

namespace sofa::component::collision::response::contact
{
template <class TCollisionModel1, class TCollisionModel2>
class CuttingContact : public core::collision::Contact
{
public:
  SOFA_CLASS(SOFA_TEMPLATE2(CuttingContact, TCollisionModel1, TCollisionModel2), core::collision::Contact);
  typedef TCollisionModel1 CollisionModel1;
  typedef TCollisionModel2 CollisionModel2;

  typedef typename CollisionModel1::Element CollisionElement1;
  typedef typename CollisionModel2::Element CollisionElement2;

  typedef core::collision::Intersection Intersection;

  typedef core::collision::DetectionOutputVector OutputVector;
  typedef core::collision::TDetectionOutputVector<CollisionModel1,CollisionModel2> TOutputVector;
  typedef core::collision::DetectionOutput DetectionOutput;

protected:
  CollisionModel1* model1;
  CollisionModel2* model2;

  std::vector< core::collision::DetectionOutput >  detections;
  
  core::objectmodel::BaseContext* parent;

  CuttingContact();
  CuttingContact(CollisionModel1* model1, CollisionModel2* model2, Intersection* intersectionMethod);

  ~CuttingContact() override;

public:
  void cleanup() override;
  
  std::pair<core::CollisionModel*,core::CollisionModel*> getCollisionModels() override { return std::make_pair(model1,model2); }
  
  void setDetectionOutputs(OutputVector* outputs) override;
  
  void createResponse(core::objectmodel::BaseContext* group) override;
  
  void removeResponse() override;
};

} // namespace sofa::component::collision::response::contact

